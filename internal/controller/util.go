package controller

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// NotFoundError represents that an object was not found.
type NotFoundError struct {
}

// Error implements the error interface.
func (e *NotFoundError) Error() string {
	return "Object not found"
}

// hasAnnotation returns true if the object has the specified annotation.
func HasAnnotation(o metav1.Object, annotation string) bool {
	annotations := o.GetAnnotations()
	if annotations == nil {
		return false
	}
	_, ok := annotations[annotation]
	return ok
}

// Filter filters a list for a string.
func Filter(list []string, strToFilter string) (newList []string) {
	for _, item := range list {
		if item != strToFilter {
			newList = append(newList, item)
		}
	}
	return
}

// Contains returns true if a list contains a string.
func Contains(list []string, strToSearch string) bool {
	for _, item := range list {
		if item == strToSearch {
			return true
		}
	}
	return false
}
