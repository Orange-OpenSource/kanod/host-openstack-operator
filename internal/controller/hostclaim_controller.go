/*
Copyright 2025 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"time"

	"github.com/pkg/errors"
	openstackv1alpha1 "gitlab.com/Orange-OpenSource/kanod/host-openstack-operator/internal/openstack-resource-controller/api/v1alpha1"
	hostv1alpha1 "gitlab.com/Orange-OpenSource/kanod/hostclaim/api/v1alpha1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"
	"sigs.k8s.io/cluster-api/util/conditions"
	"sigs.k8s.io/cluster-api/util/patch"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	logr "sigs.k8s.io/controller-runtime/pkg/log"
)

const (
	OpenstackAnnotation = "metal3.io/OpenstackHost"
	requeueAfter        = time.Second * 30
	// AssociateOpenstackCondition documents the status of associated the Host with a BaremetalHost.
	AssociateOpenstackCondition clusterv1.ConditionType = "AssociateOpenstack"
	// AssociateOpenstackFailedReason documents any errors while associating Host with a BaremetalHost.
	AssociateOpenstackFailedReason = "AssociateOpenstackFailed"
	// MissingOpenstackVMReason used when Openstack VM is not yet accessible
	MissingOpenstackVMReason = "MissingOpenstackVM"
)

var hasRequeueAfterError HasRequeueAfterError

// HostClaimReconciler reconciles a HostClaim object
type HostClaimReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//nolint:lll
// +kubebuilder:rbac:groups=kanod.io,resources=hostclaims,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=kanod.io,resources=hostclaims/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=kanod.io,resources=hostclaims/finalizers,verbs=update
// +kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch
// +kubebuilder:rbac:groups=openstack.k-orc.cloud,resources=openstackservers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=openstack.k-orc.cloud,resources=openstackports,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=openstack.k-orc.cloud,resources=openstackimages,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the HostClaim object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.19.4/pkg/reconcile
func (r *HostClaimReconciler) Reconcile(ctx context.Context, req ctrl.Request) (_ ctrl.Result, rerr error) {
	log := logr.FromContext(ctx).WithName("host-openstack-controller").WithValues("hostclaim", req.NamespacedName)

	host := &hostv1alpha1.HostClaim{}
	if err := r.Client.Get(ctx, req.NamespacedName, host); err != nil {
		if apierrors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	if host.Spec.Kind != "openstack" {
		// Not handled by this controller.
		log.V(1).Info("Skipping not associated to openstack.")
		return ctrl.Result{}, nil
	}
	patchHelper, err := patch.NewHelper(host, r.Client)
	if err != nil {
		return ctrl.Result{}, errors.Wrap(err, "failed to init patch helper")
	}
	defer func() {
		if err := patchHost(ctx, patchHelper, host); err != nil {
			log.Error(err, "failed to Patch metal3Machine")
			rerr = err
		}
	}()
	// clear an error if one was previously set
	clearErrorHost(host)

	// Create a helper for managing the openstack resource hosting the machine.
	hostMgr, err := NewHostManager(r.Client, log, host)
	if err != nil {
		return ctrl.Result{}, errors.Wrapf(err, "failed to create helper for managing the hostMgr")
	}

	if HasAnnotation(host, hostv1alpha1.PausedAnnotation) {
		// set pause annotation on associated  (if any)
		err := hostMgr.SetPauseAnnotation(ctx)
		if err != nil {
			log.Info("failed to set pause annotation on associated bmh")
			conditions.MarkFalse(
				host, AssociateOpenstackCondition,
				hostv1alpha1.PauseAnnotationSetFailedReason,
				clusterv1.ConditionSeverityInfo, "")
			return ctrl.Result{}, nil
		}
		conditions.MarkFalse(
			host, AssociateOpenstackCondition,
			hostv1alpha1.HostPausedReason,
			clusterv1.ConditionSeverityInfo, "")
		return ctrl.Result{Requeue: true, RequeueAfter: requeueAfter}, nil
	} else {
		err := hostMgr.RemovePauseAnnotation(ctx)
		if err != nil {
			log.Info("failed to check pause annotation on associated")
			conditions.MarkFalse(
				host, AssociateOpenstackCondition,
				hostv1alpha1.PauseAnnotationRemoveFailedReason,
				clusterv1.ConditionSeverityInfo, "")
			return ctrl.Result{}, nil
		}
	}
	// Handle deleted machines
	if !host.ObjectMeta.DeletionTimestamp.IsZero() {
		return r.reconcileDelete(ctx, hostMgr)
	}

	// Handle non-deleted machines
	return r.reconcileNormal(ctx, hostMgr)
}

func (r *HostClaimReconciler) reconcileNormal(ctx context.Context,
	hostMgr HostManagerInterface,
) (ctrl.Result, error) {
	// If the Host doesn't have finalizer, add it.
	hostMgr.SetFinalizer()

	// if the machine is already provisioned, update and return
	if hostMgr.IsProvisioned() {
		errType := hostv1alpha1.UpdateHostError
		return checkMachineError(hostMgr, hostMgr.Update(ctx),
			"Failed to update the Host", errType,
		)
	}

	errType := hostv1alpha1.CreateHostError

	// Check if the metal3machine was associated with an openstack VM
	if !hostMgr.HasAnnotation() {
		// Associate to a new VM hosting the machine
		err := hostMgr.Associate(ctx)
		if err != nil {
			hostMgr.SetConditionHostToFalse(AssociateOpenstackCondition, AssociateOpenstackFailedReason,
				clusterv1.ConditionSeverityError, err.Error())
			return checkMachineError(hostMgr, err,
				"failed to create an openstack server for the Host", errType,
			)
		}
	}
	// Update Condition to reflect that we have an associated VM
	hostMgr.SetConditionHostToTrue(AssociateOpenstackCondition)

	err := hostMgr.Update(ctx)
	if err != nil {
		return checkMachineError(hostMgr, err,
			"failed to update Host", errType,
		)
	}

	_, err = hostMgr.GetHostID(ctx)
	if err != nil {
		hostMgr.SetConditionHostToFalse(AssociateOpenstackCondition, MissingOpenstackVMReason,
			clusterv1.ConditionSeverityError, err.Error())
		return checkMachineError(hostMgr, err,
			"failed to get the providerID for the metal3machine", errType,
		)
	}
	return ctrl.Result{}, err
}

func (r *HostClaimReconciler) reconcileDelete(ctx context.Context,
	hostMgr HostManagerInterface,
) (ctrl.Result, error) {
	// set machine condition to Deleting
	hostMgr.SetConditionHostToFalse(AssociateOpenstackCondition, hostv1alpha1.DeletingReason,
		clusterv1.ConditionSeverityInfo, "")

	errType := hostv1alpha1.DeleteHostError

	// delete the machine
	if err := hostMgr.Delete(ctx); err != nil {
		hostMgr.SetConditionHostToFalse(AssociateOpenstackCondition, hostv1alpha1.DeletionFailedReason,
			clusterv1.ConditionSeverityWarning, err.Error())
		return checkMachineError(hostMgr, err,
			"failed to delete Host", errType,
		)
	}

	// metal3machine is marked for deletion and ready to be deleted,
	// so remove the finalizer.
	hostMgr.UnsetFinalizer()

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *HostClaimReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&hostv1alpha1.HostClaim{}).
		Watches(
			&openstackv1alpha1.OpenStackServer{},
			handler.EnqueueRequestsFromMapFunc(r.OpenstackServerToHost)).
		//		Owns(&openstackv1alpha1.OpenStackServer{}).
		Complete(r)
}

// BareMetalHostToMetal3Machines will return a reconcile request for a Metal3Machine if the event is for a
// BareMetalHost and that BareMetalHost references a Metal3Machine.
func (r *HostClaimReconciler) OpenstackServerToHost(_ context.Context, obj client.Object) []ctrl.Request {
	log := logr.Log.WithName("host-openstack-controller")
	if host, ok := obj.(*openstackv1alpha1.OpenStackServer); ok {
		if host.Labels != nil {
			if hostName, ok := host.Labels[ServerAnnotation]; ok {
				log.V(1).Info("Notifying Host", "host", hostName, "namespace", host.Namespace)
				return []ctrl.Request{
					{NamespacedName: types.NamespacedName{
						Name:      hostName,
						Namespace: host.Namespace,
					}},
				}
			}
		}
		return []ctrl.Request{}
	} else {
		log.Error(errors.Errorf("expected an OpenstackServer but got a %T", obj),
			"failed to get Host for OpenstackServer",
		)
	}
	return []ctrl.Request{}
}

func patchHost(
	ctx context.Context,
	patchHelper *patch.Helper,
	host *hostv1alpha1.HostClaim,
	options ...patch.Option,
) error {
	logr.Log.V(1).Info("Patching host", "name", host.Name)
	// Always update the readyCondition by summarizing the state of other conditions.
	conditions.SetSummary(host,
		conditions.WithConditions(
			AssociateOpenstackCondition,
		),
	)

	// Patch the object, ignoring conflicts on the conditions owned by this controller.
	options = append(
		options,
		patch.WithOwnedConditions{Conditions: []clusterv1.ConditionType{
			clusterv1.ReadyCondition,
			AssociateOpenstackCondition,
		}},
		patch.WithStatusObservedGeneration{},
	)
	return patchHelper.Patch(ctx, host, options...)
}

// clearError removes the ErrorMessage from the metal3machine's Status if set.
func clearErrorHost(host *hostv1alpha1.HostClaim) {
	if host.Status.FailureMessage != nil || host.Status.FailureReason != nil {
		host.Status.FailureMessage = nil
		host.Status.FailureReason = nil
	}
}

func checkMachineError(hostMgr HostManagerInterface, err error,
	errMessage string, errType hostv1alpha1.HostStatusError,
) (ctrl.Result, error) {
	if err == nil {
		return ctrl.Result{}, nil
	}
	if ok := errors.As(err, &hasRequeueAfterError); ok {
		return ctrl.Result{Requeue: true, RequeueAfter: hasRequeueAfterError.GetRequeueAfter()}, nil
	}
	hostMgr.SetError(errMessage, errType)
	return ctrl.Result{}, errors.Wrap(err, errMessage)
}
