package controller

import (
	"context"
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"regexp"
	"strings"
	"sync"

	"github.com/go-logr/logr"
	"github.com/pkg/errors"
	openstackv1alpha1 "gitlab.com/Orange-OpenSource/kanod/host-openstack-operator/internal/openstack-resource-controller/api/v1alpha1"
	hostv1alpha1 "gitlab.com/Orange-OpenSource/kanod/hostclaim/api/v1alpha1"
	"gopkg.in/yaml.v2"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/equality"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/tools/cache"
	"k8s.io/utils/ptr"
	clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"
	"sigs.k8s.io/cluster-api/util/conditions"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

type HostManagerInterface interface {
	SetFinalizer()
	UnsetFinalizer()
	IsProvisioned() bool
	Associate(context.Context) error
	Delete(context.Context) error
	Update(context.Context) error
	SetError(string, hostv1alpha1.HostStatusError)
	SetPauseAnnotation(context.Context) error
	RemovePauseAnnotation(context.Context) error
	GetHostID(context.Context) (*string, error)
	HasAnnotation() bool
	SetConditionHostToFalse(clusterv1.ConditionType, string, clusterv1.ConditionSeverity, string, ...interface{})
	SetConditionHostToTrue(clusterv1.ConditionType)
}

type OpenstackHostManager struct {
	client client.Client
	Host   *hostv1alpha1.HostClaim
	Log    logr.Logger
}

const (
	// HostAnnotation is the key for an annotation that should go on a Host to
	// reference what Openstack server it corresponds to.
	HostAnnotation = "metal3.io/OpenstackHost"
	// Server annotation is the annotation on the server pointing to
	// the host it is linked to.
	ServerAnnotation = "host.kanod.io/host"
	// CloudLabel is a mandatory label containing the name of the OpenstackCloud
	CloudLabel = "host.kanod.io/openstack-cloud"
	// FlavorLabel is a mandatory label containing the flavor of the OpenstackCloud
	FlavorLabel = "host.kanod.io/openstack-flavor"
	// NetworksLabel is a mandatory ** fake ** label containing the connected networks
	// The content is a comma separated list of networks. As such it does not
	// fulfil the requirements for a label.
	NetworksLabel = "host.kanod.io/openstack-networks"
	// UnhealthyAnnotation is the annotation used by the Metal3Health
	// that sets unhealthy status of server.
	UnhealthyAnnotation = "capi.metal3.io/unhealthy"

	ImageUrlLabel = "openstack.kanod.io/image-url"
)

var (
	// a mutex to perform one association at a time
	associateMutex sync.Mutex
	// a regexp to skip/select labels that are handled by the host operator.
	matchHostDomainRe = regexp.MustCompile(`^host.kanod.io/`)
)

func NewHostManager(
	client client.Client,
	log logr.Logger,
	host *hostv1alpha1.HostClaim,
) (*OpenstackHostManager, error) {
	return &OpenstackHostManager{
		client: client,
		Host:   host,
		Log:    log,
	}, nil
}

// SetError sets the ErrorMessage and ErrorReason fields on the machine and logs
// the message. It assumes the reason is invalid configuration, since that is
// currently the only relevant MachineStatusError choice.
func (m *OpenstackHostManager) SetError(message string, reason hostv1alpha1.HostStatusError) {
	m.Host.Status.FailureMessage = &message
	m.Host.Status.FailureReason = &reason
}

// clearError removes the ErrorMessage from the machine's Status if set. Returns
// nil if ErrorMessage was already nil. Returns a RequeueAfterError if the
// machine was updated.
func (m *OpenstackHostManager) clearError() {
	if m.Host.Status.FailureMessage != nil || m.Host.Status.FailureReason != nil {
		m.Host.Status.FailureMessage = nil
		m.Host.Status.FailureReason = nil
	}
}

func (m *OpenstackHostManager) SetPauseAnnotation(ctx context.Context) error {
	return nil
}

func (m *OpenstackHostManager) RemovePauseAnnotation(ctx context.Context) error {
	return nil
}

// getServer gets the associated Openstack server by looking for an annotation on the machine
// that contains a reference to the host. Returns nil if not found. Assumes the
// host is in the same namespace as the machine.
func (m *OpenstackHostManager) getServer(
	ctx context.Context,
) (*openstackv1alpha1.OpenStackServer, error) {
	host, err := getServer(ctx, m.Host, m.client, m.Log)
	if err != nil || host == nil {
		return host, err
	}
	return host, err
}

func getServer(ctx context.Context, m3Machine *hostv1alpha1.HostClaim, cl client.Client,
	mLog logr.Logger,
) (*openstackv1alpha1.OpenStackServer, error) {
	annotations := m3Machine.ObjectMeta.GetAnnotations()
	if annotations == nil {
		return nil, nil
	}
	hostKey, ok := annotations[OpenstackAnnotation]
	if !ok {
		return nil, nil
	}
	hostNamespace, hostName, err := cache.SplitMetaNamespaceKey(hostKey)
	if err != nil {
		mLog.Error(err, "Error parsing annotation value", "annotation key", hostKey)
		return nil, err
	}

	host := openstackv1alpha1.OpenStackServer{}
	key := client.ObjectKey{
		Name:      hostName,
		Namespace: hostNamespace,
	}
	err = cl.Get(ctx, key, &host)
	if apierrors.IsNotFound(err) {
		mLog.Info("Annotated host not found", "host", hostKey)
		return nil, nil
	} else if err != nil {
		mLog.Error(err, "failure while retrieving openstack server")
		return nil, err
	}
	return &host, nil
}

// SetFinalizer sets finalizer on the host
func (m *OpenstackHostManager) SetFinalizer() {
	// If the Host doesn't have finalizer, add it.
	if !Contains(m.Host.Finalizers, hostv1alpha1.HostFinalizer) {
		m.Host.Finalizers = append(m.Host.Finalizers,
			hostv1alpha1.HostFinalizer,
		)
	}
}

// UnsetFinalizer unsets finalizer on the host
func (m *OpenstackHostManager) UnsetFinalizer() {
	// Cluster is deleted so remove the finalizer.
	m.Host.Finalizers = Filter(m.Host.Finalizers,
		hostv1alpha1.HostFinalizer,
	)
}

func (m *OpenstackHostManager) IsProvisioned() bool {
	return m.Host.Status.Ready
}

func (m *OpenstackHostManager) Associate(ctx context.Context) error {
	associateMutex.Lock()
	defer associateMutex.Unlock()
	m.Log.Info("Associating host", "host", m.Host.Name)

	// load and validate the config
	if m.Host == nil {
		// Should have been picked earlier. Do not requeue
		return nil
	}
	// clear an error if one was previously set
	m.clearError()

	// The way Openstack server are launched, we need to wait until it is
	// declared online.
	if !m.Host.Spec.Online {
		return nil
	}
	var cloud, flavor, networksString string
	var ok bool
	specLabels := m.Host.Spec.HostSelector.MatchLabels
	if cloud, ok = specLabels[CloudLabel]; !ok {
		m.SetError(
			"Specifying a cloud is required",
			hostv1alpha1.CreateHostError)
		return fmt.Errorf("no cloud specified")
	}
	if flavor, ok = specLabels[FlavorLabel]; !ok {
		m.SetError(
			"Specifying a flavor is required",
			hostv1alpha1.CreateHostError)
		return fmt.Errorf("no flavor specified")
	}
	if networksString, ok = specLabels[NetworksLabel]; !ok {
		m.SetError(
			"Specifying at least one network is required",
			hostv1alpha1.CreateHostError)
		return fmt.Errorf("no network connected")
	}
	var labels = map[string]string{}
	labels[ServerAnnotation] = m.Host.Name
	for labelKey, labelVal := range m.Host.Spec.HostSelector.MatchLabels {
		if matchHostDomainRe.MatchString(labelKey) {
			continue
		}
		labels[labelKey] = labelVal
	}

	networks := strings.Split(networksString, ",")
	openstackNetworks := make([]openstackv1alpha1.OpenStackServerSpecNetworks, len(networks))
	for i, network := range networks {
		if strings.Contains(network, ":") {
			portName, err := m.CreatePort(ctx, cloud, i, network)
			if err != nil {
				return err
			}
			openstackNetworks[i] = openstackv1alpha1.OpenStackServerSpecNetworks{
				Port: portName,
			}
		} else {
			openstackNetworks[i] = openstackv1alpha1.OpenStackServerSpecNetworks{
				Network: network,
			}
		}
	}
	userDataSecret, err := m.getSecret(ctx, m.Host.Spec.UserData.Name)
	if err != nil {
		return fmt.Errorf("no user data: %s", err.Error())
	}
	userData, ok := userDataSecret["value"]
	if !ok {
		return fmt.Errorf("no user data: missing userData field")
	}
	metaData := map[string]string{}
	if m.Host.Spec.MetaData.Name != "" {
		metaDataSecret, err := m.getSecret(ctx, m.Host.Spec.MetaData.Name)
		if err != nil {
			return fmt.Errorf("no meta data: %s", err.Error())
		}
		metaDataContent, ok := metaDataSecret["metaData"]
		if !ok {
			return fmt.Errorf("no meta data: missing metaData field")
		}
		err = yaml.Unmarshal(metaDataContent, &metaData)
		if err != nil {
			m.Log.Error(err, "cannot unmarshal metadata")
			return fmt.Errorf("cannot parse metadata")
		}
	}
	image, err := m.getImage(ctx, m.Host.Spec.Image, cloud)
	if err != nil {
		return fmt.Errorf("cannot get image: %s", err.Error())
	}
	server := &openstackv1alpha1.OpenStackServer{
		ObjectMeta: metav1.ObjectMeta{
			Name:      m.Host.Name,
			Namespace: m.Host.Namespace,
			Labels:    labels,
		},
		Spec: openstackv1alpha1.OpenStackServerSpec{
			CommonSpec: openstackv1alpha1.CommonSpec{
				Cloud: cloud,
			},
			Resource: &openstackv1alpha1.OpenStackServerResourceSpec{
				Flavor:      flavor,
				Networks:    openstackNetworks,
				UserData:    string(userData),
				Metadata:    metaData,
				Image:       image,
				Name:        m.Host.Name,
				ConfigDrive: true,
			},
		},
	}
	err = controllerutil.SetOwnerReference(m.Host, server, m.client.Scheme())
	if err != nil {
		m.Log.Error(err, "cannot set OwnerReference")
		return err
	}

	err = m.client.Create(ctx, server)
	if err != nil {
		return err
	}

	err = m.ensureAnnotation(server)
	if err != nil {
		if ok := errors.As(err, &hasRequeueAfterError); !ok {
			m.SetError("Failed to annotate the Host",
				hostv1alpha1.CreateHostError,
			)
		}
		return err
	}

	m.Log.Info("Finished associating machine")
	return nil
}

// Update updates a machine and is invoked by the Machine Controller.
func (m *OpenstackHostManager) Update(ctx context.Context) error {
	m.Log.Info("Updating machine")

	// clear any error message that was previously set. This method doesn't set
	// error messages yet, so we know that it's incorrect to have one here.
	m.clearError()

	if !m.Host.Spec.Online {
		m.Log.V(1).Info("Host not online. No update but declare it as synced")
		m.Host.Status.Synced = true
		return nil
	}

	m.Host.Status.Synced = false
	server, err := m.getServer(ctx)
	if err != nil {
		return err
	}
	if server == nil {
		return errors.Errorf("Openstack server not found for host %s", m.Host.Name)
	}

	err = m.ensureAnnotation(server)
	if err != nil {
		return err
	}

	if err := m.updateHostStatus(server); err != nil {
		return err
	}
	m.Host.Status.Synced = true

	m.Log.Info("Finished updating machine")
	return nil
}

// SetConditionHostToFalse sets Host condition status to False.
func (m *OpenstackHostManager) SetConditionHostToFalse(
	t clusterv1.ConditionType,
	reason string,
	severity clusterv1.ConditionSeverity,
	messageFormat string,
	messageArgs ...interface{},
) {
	conditions.MarkFalse(m.Host, t, reason, severity, messageFormat, messageArgs...)
}

// SetConditionHostToTrue sets Host condition status to True.
func (m *OpenstackHostManager) SetConditionHostToTrue(t clusterv1.ConditionType) {
	conditions.MarkTrue(m.Host, t)
}

// ensureAnnotation makes sure the machine has an annotation that references the
// host and uses the API to update the machine if necessary.
func (m *OpenstackHostManager) ensureAnnotation(host *openstackv1alpha1.OpenStackServer) error {
	annotations := m.Host.ObjectMeta.GetAnnotations()
	if annotations == nil {
		annotations = make(map[string]string)
	}
	hostKey, err := cache.MetaNamespaceKeyFunc(host)
	if err != nil {
		m.Log.Error(err, "Error parsing annotation value", "annotation key", hostKey)
		return err
	}
	existing, ok := annotations[OpenstackAnnotation]
	if ok {
		if existing == hostKey {
			return nil
		}
		m.Log.Info("Warning: found stray annotation for host on machine. Overwriting.", "host", existing)
	}
	annotations[OpenstackAnnotation] = hostKey
	m.Host.ObjectMeta.SetAnnotations(annotations)

	return nil
}

// updateHostStatus updates a Host object's status.
func (m *OpenstackHostManager) updateHostStatus(server *openstackv1alpha1.OpenStackServer) error {
	addrs := m.nodeAddresses(server)
	hostOld := m.Host.DeepCopy()

	m.Host.Status.Addresses = addrs
	conditions.MarkTrue(m.Host, AssociateOpenstackCondition)

	if equality.Semantic.DeepEqual(m.Host.Status, hostOld.Status) {
		// Status did not change
		return nil
	}

	now := metav1.Now()
	m.Host.Status.LastUpdated = &now
	return nil
}

func (m *OpenstackHostManager) nodeAddresses(
	server *openstackv1alpha1.OpenStackServer,
) []hostv1alpha1.HostAddress {
	addrs := []hostv1alpha1.HostAddress{}
	// If the host is nil or we have no hw details, return an empty address array.
	if server == nil {
		return addrs
	}
	statusAddrs := map[string][]map[string]interface{}{}
	err := json.Unmarshal([]byte(server.Status.Resource.Addresses), &statusAddrs)
	if err != nil {
		m.Log.Error(err, "cannot parse addresses", "addresses", server.Status.Resource.Addresses)
		return addrs
	}
	for _, addrBloc := range statusAddrs {
		for _, addrElt := range addrBloc {
			addr, ok := addrElt["addr"]
			if !ok {
				continue
			}
			address := hostv1alpha1.HostAddress{
				Type:    hostv1alpha1.HostInternalIP,
				Address: addr.(string),
			}
			addrs = append(addrs, address)
		}
	}

	return addrs
}

// GetHostID return the provider identifier for this machine when the
// host is provisionned otherwise return nil or an error.
func (m *OpenstackHostManager) GetHostID(ctx context.Context) (*string, error) {
	// look for associated server
	server, err := m.getServer(ctx)
	if err != nil {
		m.SetError("Failed to get an Openstack server for the Host",
			hostv1alpha1.CreateHostError,
		)
		return nil, err
	}
	if server == nil {
		m.Log.Info("Host not associated, requeuing")
		return nil, &RequeueAfterError{RequeueAfter: requeueAfter}
	}
	if server.Status.Resource.Status == "ACTIVE" {
		serverID := string(server.ObjectMeta.UID)
		m.Host.Status.HostUID = serverID
		m.Host.Status.Ready = true
		m.Log.Info("Setting HostUID", "hostUID", serverID)
		return ptr.To(serverID), nil
	} else {
		m.Host.Status.Ready = false
	}
	m.Log.Info("Provisioning Openstack server, requeuing")
	// Do not requeue since server update will trigger a reconciliation
	return nil, nil
}

// HasAnnotation makes sure the host has an annotation that references a host.
func (m *OpenstackHostManager) HasAnnotation() bool {
	annotations := m.Host.ObjectMeta.GetAnnotations()
	if annotations == nil {
		return false
	}
	_, ok := annotations[HostAnnotation]
	return ok
}

// Delete deletes a metal3 machine and is invoked by the Machine Controller.
func (m *OpenstackHostManager) Delete(ctx context.Context) error {
	m.Log.Info("Deleting host machine", "host", m.Host.Name)

	// clear an error if one was previously set.
	m.clearError()

	server, err := m.getServer(ctx)
	if err != nil {
		return err
	}
	if server == nil {
		m.Log.Info("host not found for host", "host", m.Host.Name)
		return nil
	}
	m.Log.Info("finished deleting openstack machine")
	return nil
}

func (m *OpenstackHostManager) getSecret(ctx context.Context, name string) (map[string][]byte, error) {
	secret := &corev1.Secret{}
	if err := m.client.Get(ctx, types.NamespacedName{Name: name, Namespace: m.Host.Namespace}, secret); err != nil {
		return nil, err
	}
	return secret.Data, nil
}

func (m *OpenstackHostManager) getImage(ctx context.Context, img *hostv1alpha1.Image, cloud string) (string, error) {
	url := img.URL
	hash := sha1.Sum([]byte(url))
	hashString := fmt.Sprintf("%x", hash)
	imageName := "img-" + hashString[:10]
	m.Log.Info("trying to check image", "image", url, "imageName", imageName)
	var imageList openstackv1alpha1.OpenStackImageList
	err := m.client.List(ctx, &imageList, client.MatchingLabels{ImageUrlLabel: hashString})
	if err != nil {
		m.Log.Error(err, "cannot list images")
		return "", err
	}
	switch len(imageList.Items) {
	case 1:
		return imageList.Items[0].Name, nil
	case 0:
		{
			image := openstackv1alpha1.OpenStackImage{
				ObjectMeta: metav1.ObjectMeta{
					Name:      imageName,
					Namespace: m.Host.Namespace,
					Labels: map[string]string{
						ImageUrlLabel: hashString,
					},
				},
				Spec: openstackv1alpha1.OpenStackImageSpec{
					Resource: &openstackv1alpha1.OpenStackImageResourceSpec{
						Method:          "web-download",
						Name:            imageName,
						DiskFormat:      *img.DiskFormat,
						ContainerFormat: "bare",
						WebDownload: &openstackv1alpha1.OpenStackImageResourceWebDownload{
							URL: url,
						},
					},
					CommonSpec: openstackv1alpha1.CommonSpec{
						Cloud: cloud,
					},
				},
			}
			m.Log.Info("creating image", "image", img.URL)
			err := m.client.Create(ctx, &image)
			if err != nil {
				m.Log.Error(err, "image creation failed")
				return "", err
			}
			return imageName, nil
		}
	default:
		return "", fmt.Errorf("multiple images attached to same URL")
	}
}

func (m *OpenstackHostManager) CreatePort(ctx context.Context, cloud string, i int, spec string) (string, error) {
	elt := strings.Split(spec, ":")
	if len(elt) < 3 {
		return "", fmt.Errorf("not enough components for network specification %d with security group: %s", i, spec)
	}
	network := elt[0]
	subnet := elt[1]
	securityGroups := elt[2:]
	name := fmt.Sprintf("%s-%d", m.Host.Name, i)
	port := &openstackv1alpha1.OpenStackPort{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: m.Host.Namespace,
		},
		Spec: openstackv1alpha1.OpenStackPortSpec{
			CommonSpec: openstackv1alpha1.CommonSpec{
				Cloud: cloud,
			},
			Resource: &openstackv1alpha1.OpenStackPortResourceSpec{
				Network:        network,
				SecurityGroups: securityGroups,
				FixedIPs: []openstackv1alpha1.FixedIP{
					{
						Subnet: subnet,
					},
				},
			},
		},
	}
	err := controllerutil.SetOwnerReference(m.Host, port, m.client.Scheme())
	if err != nil {
		m.Log.Error(err, "cannot set OwnerReference")
		return "", err
	}

	err = m.client.Create(ctx, port)
	if err != nil {
		return "", err
	}
	return name, nil
}
