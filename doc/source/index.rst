==============
OpenStack Host
==============

The OpenStack Host resource controller maps Host Resource to
OpenStack virtual machines. The virtual machine is a disposable compute
resource: it is created for the Host and destroyed as soon as the host
is destroyed.

Host Selector
=============
The Host resource must contain a label selector set for ``host.kanod.io/kind``
to ``openstack``. The generated resource is expressed
using `k-orc <https://k-orc.cloud>`_ custom resources. The goal of the k-orc
project is to represent standard OpenStack concepts as Kubernetes resources.
It is still in alpha stage and we recomend to use the Kanod fork.

Resource configuration
======================
To operate, the OpenStack virtual machine needs access to a set of companion
resources. All the resources must be in the same namespace and are k-orc objects:

* OpenStack credentials: ``host.kanod.io/openstack-cloud`` must contain the name
  of an OpenStackCloud resource.
* The dimension of the server represented as an OpenStack flavour ``host.kanod.io/openstack-flavor``
  should contain the name of an OpenstackFlavor resource.
* The networks connected to the server: ``host.kanod.io/openstack-networks`` must be associated
  to a comma separated list of OpenStackNetwork resource names. 

All the resource must be compatible: they should use the same OpenstackCloud
resource.

OpenStack Host known quirks
===========================
Meta-data from the host are handled by the OpenStack server but for security
purpose, an additional ``meta`` layer is added, so a meta-data named ``key``
will be available as ``{{ ds.meta_data.meta.key }}`` in cloud-init.

The OS image must be imported into OpenStack Glance. The operation is automatically
performed by the controller and it will try to cache the imported images using 
a hash of the URL to name them. But it will not garbage collect them when they
are not used.
